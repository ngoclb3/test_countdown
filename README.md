### Installation

Install the dependencies and devDependencies.

```sh
$ npm install
```

#### Run app

Open your terminal and run these commands.

Start the server:
```sh
$ npm start
```

Run android (Debug):
```sh
$ npx react-native run-android
```

Run android (Release):
```sh
$ npx react-native run-android --variant=release
```