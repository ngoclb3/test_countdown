import { PixelRatio, Dimensions, Platform } from 'react-native'
const { width, height } = Dimensions.get('window')
export default class Constants {
    static IS_IOS = Platform.OS == "ios" ? true : false;

    // Dimensions
    static dvWidth = width;
    static dvHeight = height;
    static px = PixelRatio.get() / 3.0;

    // Table Color
    static primary = '#1292B4';
    static white = '#FFF';
    static lighter = '#F3F3F3';
    static light = '#DAE1E7';
    static dark = '#444';
    static black = '#000';
}
