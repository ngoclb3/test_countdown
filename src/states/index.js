import { countDown } from "./count-down-states";

export const reducers = {
  countDown,
};

export {
  initCountDown,
  startCountDown,
  pauseCountDown,
  increaseCountDown,
  stopCountDown,
} from "./count-down-states";
