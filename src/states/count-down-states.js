// actions
const INIT_COUNT_DOWN = "INIT_COUNT_DOWN";
const START_COUNT_DOWN = "START_COUNT_DOWN";
const PAUSE_COUNT_DOWN = "PAUSE_COUNT_DOWN";
const INCREASE_COUNT_DOWN = "INCREASE_COUNT_DOWN";
const STOP_COUNT_DOWN = "STOP_COUNT_DOWN";

const initialState = {
  hours: "00",
  minutes: "00",
  seconds: "00",
  total_timer: 0,
  status: "INIT_COUNT_DOWN"
}

// action creators
export const initCountDown = () => ({
  type: INIT_COUNT_DOWN,
});
export const startCountDown = () => ({
  type: START_COUNT_DOWN,
});
export const pauseCountDown = () => ({
  type: PAUSE_COUNT_DOWN,
});
export const increaseCountDown = (timeAdd) => ({
  type: INCREASE_COUNT_DOWN,
  timeAdd,
});
export const stopCountDown = () => ({
  type: STOP_COUNT_DOWN,
});

// reducer
export const countDown = (state = {}, action) => {
  switch (action.type) {
    case INIT_COUNT_DOWN:
      state = initialState;
      break;
    case START_COUNT_DOWN:
      let total_timer = state.total_timer - 1;
      let hours = Math.floor(total_timer / 3600);
      total_timer %= 3600;
      let minutes = Math.floor(total_timer / 60);
      let seconds = total_timer % 60;
      minutes = String(minutes).padStart(2, "0");
      hours = String(hours).padStart(2, "0");
      seconds = String(seconds).padStart(2, "0");
      return Object.assign(
        {},
        state,
        {
          total_timer: state.total_timer - 1,
          hours: hours,
          minutes: minutes,
          seconds: seconds,
          status: "START_COUNT_DOWN"
        }
      );
    case PAUSE_COUNT_DOWN:
      let total_timer_pause = state.total_timer;
      let hours_pause = Math.floor(total_timer_pause / 3600);
      total_timer_pause %= 3600;
      let minutes_pause = Math.floor(total_timer_pause / 60);
      let seconds_pause = total_timer_pause % 60;
      minutes_pause = String(minutes_pause).padStart(2, "0");
      hours_pause = String(hours_pause).padStart(2, "0");
      seconds_pause = String(seconds_pause).padStart(2, "0");
      return Object.assign(
        {},
        state,
        {
          total_timer: state.total_timer,
          hours: hours_pause,
          minutes: minutes_pause,
          seconds: seconds_pause,
          status: "PAUSE_COUNT_DOWN"
        }
      );
    case INCREASE_COUNT_DOWN:
      let total_timer_add = state.total_timer + action.timeAdd;
      let hours_add = Math.floor(total_timer_add / 3600);
      total_timer_add %= 3600;
      let minutes_add = Math.floor(total_timer_add / 60);
      let seconds_add = total_timer_add % 60;
      minutes_add = String(minutes_add).padStart(2, "0");
      hours_add = String(hours_add).padStart(2, "0");
      seconds_add = String(seconds_add).padStart(2, "0");
      return Object.assign(
        {},
        state,
        {
          total_timer: state.total_timer + action.timeAdd,
          hours: hours_add,
          minutes: minutes_add,
          seconds: seconds_add,
          status: "INCREASE_COUNT_DOWN"
        }
      );
    case STOP_COUNT_DOWN:
      state = initialState;
      break;
  }
  return state;
};
