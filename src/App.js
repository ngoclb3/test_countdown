import React from 'react';
import { Provider } from "react-redux";
import { Root } from "native-base";
import { Router, Scene, Stack } from "react-native-router-flux";
import { store } from "./store";
import CountDownContainer from './feature/container/CountDownContainer';

export const App = () => {
  return (
    <Provider store={store}>
      <Root>
        <Router>
          <Stack key="root">
            <Scene
              key="CountDownComponent"
              component={CountDownContainer}
              title="CountDownComponent"
              hideNavBar="true"
              initial
            />
          </Stack>
        </Router>
      </Root>
    </Provider>
  );
};
