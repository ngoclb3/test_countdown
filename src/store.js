import { createStore, combineReducers } from "redux";
import { reducers } from "./states";

const rootReducers = combineReducers(reducers);

export const store = createStore(rootReducers);
