import { CountDownComponent } from '../components/CountDownComponent';
import { connect } from 'react-redux';
import { initCountDown, startCountDown, pauseCountDown, increaseCountDown, stopCountDown } from '../../states';

export default connect(
    state => {
        let countDown = state.countDown || {}
        return {
            countDown: countDown,
        }
    },
    dispatch => {
        return {
            initCountDown: () => {
                dispatch(initCountDown())
            },
            startCountDown: () => {
                dispatch(startCountDown())
            },
            pauseCountDown: () => {
                dispatch(pauseCountDown())
            },
            increaseCountDown: (timeAdd) => {
                dispatch(increaseCountDown(timeAdd))
            },
            stopCountDown: () => {
                dispatch(stopCountDown())
            },
        }
    }
)(CountDownComponent);
