import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text
} from 'react-native';
import { Container, Content } from 'native-base';
import CountdownCircle from 'react-native-countdown-circle';
import * as _ from 'lodash';
import { verticalScale, scale, moderateScale } from '../../share/SizeMatter';


export class CountDownComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: "INIT_COUNT_DOWN",
      total_timer: 0,
      hours: "00",
      minutes: "00",
      seconds: "00",
    }
    this.props.initCountDown();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.countDown && this.props.countDown !== nextProps.countDown) {
      // Handle stopCountDown when total_timer === 0
      if (nextProps.countDown.total_timer === 0 && nextProps.countDown.status !== "INIT_COUNT_DOWN") {
        clearInterval(this.interval);
        this.props.stopCountDown();
      }
      if (nextProps.countDown.status !== "START_COUNT_DOWN") {
        clearInterval(this.interval);
      }
      this.setState({
        total_timer: nextProps.countDown.total_timer,
        hours: nextProps.countDown.hours,
        minutes: nextProps.countDown.minutes,
        seconds: nextProps.countDown.seconds,
        status: nextProps.countDown.status
      });
    }
  }


  addTimeToTimer(type) {
    if (type === 0) {
      this.props.increaseCountDown(10 * 60);
    } else if (type === 1) {
      this.props.increaseCountDown(60);
    } else {
      this.props.increaseCountDown(15);
    }
  }

  clickStartCountDown() {
    if (Number(this.state.total_timer) >= 0) {
      this.interval = setInterval(() => {
        this.props.startCountDown();
      }, 1000);
    } else {
      this.props.stopCountDown();
    }
  }

  clickPauseCountDown() {
    this.props.pauseCountDown();
  }

  clickStopCountDown() {
    this.props.stopCountDown();
  }

  render() {
    return (

      <Container>
        <Content bounces={false} scrollEnabled={true} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.contentContainerStyle}>
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <CountdownCircle
              seconds={Number(this.state.total_timer)}
              radius={scale(150)}
              borderWidth={8}
              color={Number(this.state.total_timer) > 3 ? "green" : "red"}
              bgColor="#fff"
              textStyle={{ fontSize: 20 }}
              onTimeElapsed={() => { }}
              updateText={() => { }}
            />
            <View style={{ position: "absolute" }}>
              <Text numberOfLines={1} style={styles.textCounter}>{this.state.hours}:{this.state.minutes}:{this.state.seconds}</Text>
              <View style={{ width: "100%", paddingHorizontal: scale(30), flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                <Text numberOfLines={1} style={styles.textCounter1}>h</Text>
                <Text numberOfLines={1} style={styles.textCounter1}>min</Text>
                <Text numberOfLines={1} style={styles.textCounter1}>sec</Text>
              </View>
            </View>
          </View>
          {/* Button Increase Time */}
          <View style={{ flexDirection: "row", height: verticalScale(150), justifyContent: "space-evenly", alignItems: "center" }}>
            <View>
              <TouchableOpacity onPress={() => this.addTimeToTimer(0)} style={{ width: scale(75), borderWidth: 0.25, borderRadius: moderateScale(20), paddingHorizontal: scale(10) }}>
                <Text numberOfLines={1} style={styles.textTimer}>+10</Text>
              </TouchableOpacity>
              <Text numberOfLines={1} style={styles.textTimer1}>min</Text>
            </View>

            <View>
              <TouchableOpacity onPress={() => this.addTimeToTimer(1)} style={{ width: scale(75), borderWidth: 0.25, borderRadius: moderateScale(20), paddingHorizontal: scale(10) }}>
                <Text numberOfLines={1} style={styles.textTimer}>+1</Text>
              </TouchableOpacity>
              <Text numberOfLines={1} style={styles.textTimer1}>min</Text>
            </View>

            <View>
              <TouchableOpacity onPress={() => this.addTimeToTimer(2)} style={{ width: scale(75), borderWidth: 0.25, borderRadius: moderateScale(20), paddingHorizontal: scale(10) }}>
                <Text numberOfLines={1} style={styles.textTimer}>+15</Text>
              </TouchableOpacity>
              <Text numberOfLines={1} style={styles.textTimer1}>sec</Text>
            </View>
          </View>

          {/* Button Area */}
          {
            this.state.status === "INIT_COUNT_DOWN" || this.state.status === "INCREASE_COUNT_DOWN"
              ?
              <View style={{ height: verticalScale(60), justifyContent: "center", alignItems: "center", borderTopWidth: 0.25, marginHorizontal: scale(20) }}>
                <TouchableOpacity onPress={() => this.clickStartCountDown()} disabled={Number(this.state.total_timer) === 0}>
                  <Text numberOfLines={1} style={styles.start}>Start</Text>
                </TouchableOpacity>
              </View>
              :
              <View style={{ height: verticalScale(60), flexDirection: "row", justifyContent: "space-around", alignItems: "center", borderTopWidth: 0.25, marginHorizontal: scale(20), }}>
                {
                  this.state.status !== "PAUSE_COUNT_DOWN"
                    ?
                    <TouchableOpacity onPress={() => this.clickPauseCountDown()}>
                      <Text numberOfLines={1} style={styles.pause}>Pause</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={() => this.clickStartCountDown()}>
                      <Text numberOfLines={1} style={styles.pause}>Resume</Text>
                    </TouchableOpacity>
                }
                <TouchableOpacity onPress={() => this.clickStopCountDown()}>
                  <Text numberOfLines={1} style={styles.stop}>Stop</Text>
                </TouchableOpacity>
              </View>
          }
        </Content>
      </Container >
    )
  }
}

const styles = StyleSheet.create({
  contentContainerStyle: {
    flex: 1,
  },
  // Text counter
  textCounter: {
    fontSize: verticalScale(40),
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: 5,
    textAlign: "center",
    color: "black",
  },
  textCounter1: {
    fontSize: verticalScale(12),
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: 0,
    textAlign: "center",
    color: "black",
  },

  // Text style
  textTimer: {
    fontSize: verticalScale(24),
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: 0,
    textAlign: "center",
    color: "black",
  },
  textTimer1: {
    fontSize: verticalScale(14),
    fontStyle: "normal",
    fontWeight: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "gray",
  },

  // Button style
  start: {
    fontSize: verticalScale(20),
    fontStyle: "normal",
    fontWeight: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "green",
  },
  pause: {
    fontSize: verticalScale(20),
    fontStyle: "normal",
    fontWeight: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "red",
  },
  stop: {
    fontSize: verticalScale(20),
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "black"
  }
});
